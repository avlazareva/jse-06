package ru.t1.lazareva.tm.constant.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine(){
        return SCANNER.nextLine();
    }

}
